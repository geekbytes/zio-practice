import Common._
import geekbytes.sbt._


lazy val envs = createProject("environments", "zio.envs", "environments")
  .withLibraries(
    Dependencies.ZIO.modules(_.core, _.test),
    Dependencies.ZIO.Config.all
  )

lazy val app = createProject("app", "zio.test.app", "app")
  .withLibraries {
    Dependencies.ZIO.modules(_.core)
  }.dependsOn(envs)
  .settings(
    assembly / mainClass := Some("geekbytes.zio.app.ConsoleApp"),
    assembly / assemblyJarName := "zio_app.jar"
  )


inThisBuild(
  List(
    semanticdbEnabled := true,
    semanticdbVersion := scalafixSemanticdb.revision,
    scalacOptions ++= Seq("-Ywarn-unused")
  )
)