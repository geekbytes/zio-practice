package io.geekbytes.zio.config

import zio.{IO, ZIO}
import zio.config.ReadError
import zio.config.magnolia.Descriptor

trait ConfigUtils {

  def readTypeSafeConfig[Config: Descriptor]: IO[ReadError[String], Config] = {
    import zio.config._
    import zio.config.magnolia.descriptor
    import zio.config.typesafe.TypesafeConfigSource
    for {
      source <- TypesafeConfigSource.fromDefaultLoader
      description = descriptor[Config].from(source)
      config <- ZIO.fromEither(read(description))
    } yield config
  }
}

object ConfigUtils {

  trait AsMixin {
    object zioConfigUtils extends ConfigUtils
  }

}
