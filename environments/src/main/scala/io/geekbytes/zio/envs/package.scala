package io.geekbytes.zio

import zio.Has

package object envs {

  type Console = Has[Console.Service]
  type Arith   = Has[Arith.Service]

}
