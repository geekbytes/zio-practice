/*
 * Copyright (c) 2020.
 * Manu T S [http://geekbytes.io]
 */

package io.geekbytes.zio.envs

import zio.{Has, IO, Task, ULayer, URIO, ZIO, ZLayer}

import java.io.{EOFException, IOException, PrintStream}
import scala.io.StdIn
import scala.{Console => SConsole}

object Console {

  @inline def print(line: Any): URIO[Console, IO[IOException, Unit]]     = ZIO.access[Console](_.get.print(line))
  @inline def readLine: URIO[Console, Task[String]]                      = ZIO.access[Console](_.get.readLine)
  @inline def printLine(line: Any): URIO[Console, IO[IOException, Unit]] = ZIO.access[Console](_.get.printLine(line))

  trait Service {
    def print(line: Any): IO[IOException, Unit]
    def readLine: Task[String]
    def printLine(line: Any): IO[IOException, Unit]
  }

  case class Live() extends Service {
    def print(line: Any): IO[IOException, Unit] = print(SConsole.out)(line)

    def printError(line: Any): IO[IOException, Unit] = print(SConsole.err)(line)

    def printLine(line: Any): IO[IOException, Unit] = printLine(SConsole.out)(line)

    def printLineError(line: Any): IO[IOException, Unit] = printLine(SConsole.err)(line)

    //scalafix:off
    def readLine: Task[String] =
      IO {
        val line = StdIn.readLine()
        if (line ne null) line
        else throw new EOFException("There is no more input left to read")
      }
    //scalafix:on

    private def print(stream: PrintStream)(line: Any): IO[IOException, Unit] =
      IO {
        SConsole.withOut(stream)(SConsole.print(line))
      }.refineToOrDie[IOException]

    private def printLine(stream: PrintStream)(line: Any): IO[IOException, Unit] =
      IO {
        SConsole.withOut(stream)(SConsole.println(line))
      }.refineToOrDie[IOException]
  }

  val live: ULayer[Has[Service]] = ZLayer.succeed[Service](Live())

}
