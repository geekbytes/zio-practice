/*
 * Copyright (c) 2020.
 * Manu T S [http://geekbytes.io]
 */

package io.geekbytes.zio.envs

import zio.random.Random
import zio.{Has, RIO, Task, UIO, ULayer, URIO, ZIO, ZLayer}

object Arith {

  @inline def random: URIO[Arith with Random, Long]                   = RIO.accessM(_.get.random)
  @inline def add(a: Long, b: Long): URIO[Arith, Long]                = ZIO.accessM[Arith](_.get.add(a, b))
  @inline def divide(a: Long, b: Long): ZIO[Arith, Throwable, Double] = ZIO.accessM[Arith](_.get.divide(a, b))

  trait Service {
    def random: URIO[Random, Long]
    def add(a: Long, b: Long): UIO[Long]
    def divide(a: Long, b: Long): Task[Double]
  }

  class Live() extends Service {

    def random: URIO[Random, Long] = {
      ZIO.accessM[Random](_.get.nextLong)
    }

    def add(a: Long, b: Long): UIO[Long] = ZIO.succeed {
      a + b
    }

    def divide(a: Long, b: Long): Task[Double] = Task {
      a / b
    }
  }

  val live: ULayer[Has[Service]] = ZLayer.succeed[Service](new Live())

}
