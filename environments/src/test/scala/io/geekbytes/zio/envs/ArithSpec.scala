package io.geekbytes.zio.envs

import zio.{Task, ZIO}
import zio.random.Random
import zio.test.{DefaultRunnableSpec, _}
import zio.test.Assertion._

object ArithSpec extends DefaultRunnableSpec {
  override def spec = suite("test Arith")(
    testM("divide must succeed") {

      for {
        a      <- ZIO.accessM[Random](_.get.nextLong)
        b      <- ZIO.accessM[Random](_.get.nextLong)
        result <- Arith.divide(a, b).provideLayer(Arith.live)
      } yield assert(result)(equalTo[Double, Double](a / b))

    },
    testM("divide must fail on division by 0") {

      for {
        a      <- ZIO.accessM[Random](_.get.nextLong)
        result <- Arith.divide(a, 0L).provideLayer(Arith.live)
      } yield assert {
        result
      }(throws(isSubtype[ArithmeticException](anything)))

    }
  )
}
