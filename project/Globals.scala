/*
 * Copyright (c) 2020-2022 http://geekbytes.io
 * All rights reserved.
 * Author : Manu T S [manu@geekbytes.io - http://geekbytes.io]
 */

import sbt.Resolver
import sbt.librarymanagement.MavenRepository

object Globals {

  val RESOLVERS: Seq[MavenRepository] = Seq(
    Resolver.sonatypeRepo("releases"),
    Resolver.sonatypeRepo("snapshots")
  )

  val SCALA_VERSION = "2.13.6"

}
