/*
 * Copyright (c) 2020-2022 http://geekbytes.io
 * All rights reserved.
 * Author : Manu T S [manu@geekbytes.io - http://geekbytes.io]
 */

import geekbytes.sbt._
import sbt._

import scala.language.implicitConversions

//noinspection TypeAnnotation
object Dependencies extends DependenciesHelper {

  object ZIO extends DependencyGroup("dev.zio", "1.0.12") {
    val core: DependencyArtifact = "zio"

    val test = artifact("zio-test")(_ % Test)

    def all = modules(_.core)

    object Config extends SubGroup("1.0.10") {
      val core     = artifact("zio-config")
      val typesafe = artifact("zio-config-typesafe")

      def all = modules(_.core, _.typesafe)
    }
  }

  object Config {

    val PureConfig = dependencies("com.github.pureconfig" %% "pureconfig" % "0.12.2")

    val scalaConfig = dependencies("com.github.andr83" %% "scalaconfig" % "0.6")

    val typeSafeConfig = dependencies("com.typesafe" % "config" % "1.4.0")

  }

  object Tests {

    val scalaTest = dependencies("org.scalatest" %% "scalatest" % "3.1.0")

    val mockito = dependencies("org.mockito" %% "mockito-scala" % "1.11.2")

    val scalaCheck = dependencies("org.scalacheck" %% "scalacheck" % "1.14.1")

    val randomDataGen = dependencies("com.danielasfregola" %% "random-data-generator" % "2.4")

  }

  object Logging {
    val logback = dependencies("ch.qos.logback" % "logback-classic" % "1.2.3")
  }

  val Lens = dependencies("com.softwaremill.quicklens" %% "quicklens" % "1.4.12")

  val ShapeLess = dependencies("com.chuusai" %% "shapeless" % "2.3.3")

}
