/*
 * Copyright (c) 2020-2022 http://geekbytes.io
 * All rights reserved.
 * Author : Manu T S [manu@geekbytes.io - http://geekbytes.io]
 */

import geekbytes.sbt._

object Common extends DefaultProjects("io.geekbytes", "zio", Globals.SCALA_VERSION, mavenResolvers = Globals.RESOLVERS)
