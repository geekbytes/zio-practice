/*
 * Copyright (c) 2021.
 * Manu T S [http://geekbytes.io]
 */

package io.geekbytes.zio.app

import zio._

object ConsoleApp extends zio.App {

  import io.geekbytes.zio.envs.Console

  val runtime = new BootstrapRuntime {}

  val myAppLogic: ZIO[Console, Nothing, Unit] =
    for {
      _ <- Console.printLine("Hello! What is your name?")
      name <- Console.readLine
      _ <- Console.printLine(s"Hello, ${name}, welcome to ZIO!")
    } yield ()

  override def run(args: List[String]) = {

    myAppLogic.provideCustomLayer(Console.live).exitCode

  }
}

object ZioApp extends zio.App {

  val myAppLogic =
    for {
      _ <- console.putStrLn("Hello! What is your name?")
      name <- console.getStrLn
      _ <- console.putStrLn(s"Hello, ${name}, welcome to ZIO!")
    } yield ()

  override def run(args: List[String]) = {

    myAppLogic.provideCustomLayer(console.Console.live).exitCode

  }

}
